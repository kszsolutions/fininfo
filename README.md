The Finfo application is a basic application that can create employees, list employees and update employees. Once an employes is created various tasks can be added or deleted from an employee and you can retrieve a specific task for an employee or a list of tasks for an employee.

Please find below the various curl calls and results.

Add employee
curl -X POST -H 'Content-Type: application/json' -i http://localhost:8080/api/user --data '{ "firstName": "luke", "lastName": "keyser"}'

Result:
{"user_id":"17c85a78-cdbc-47cc-91ff-cc62e0acd610"}


Update employee
curl -X PUT -H 'Content-Type: application/json' -i http://localhost:8080/api/user/17c85a78-cdbc-47cc-91ff-cc62e0acd610 --data '{"firstName": "Luke_update","lastName": "Keyser_update" }'

Result:
Status Code: 200
Content-Length: 0


Get employees
curl -X GET -i http://localhost:8080/api/user --data ''

Result:
[{"userId":"17c85a78-cdbc-47cc-91ff-cc62e0acd610","firstName":"Luke_update","lastName":"Keyser_update"}]


Add a task:
curl -X POST -H 'Content-Type: application/json' -i http://localhost:8080/api/user/17c85a78-cdbc-47cc-91ff-cc62e0acd610/task --data '{"name": "luke_task","description": "task_descrp","taskDate": "20168-05-25 14:25:00"}'

Result:
{"task_id":"2f1f73d8-12e9-4b46-9c23-a437f39ebf50"}


Update Task:
curl -X PUT -H 'Content-Type: application/json' -i http://localhost:8080/api/user/17c85a78-cdbc-47cc-91ff-cc62e0acd610/task/2f1f73d8-12e9-4b46-9c23-a437f39ebf50 --data '{"name": "luketask"}'
 
Result:
Status Code: 200
Content-Length: 0

Get employees task:
curl -X GET -i http://localhost:8080/api/user/17c85a78-cdbc-47cc-91ff-cc62e0acd610/task --data ''

Result:
[{"taskId":"2f1f73d8-12e9-4b46-9c23-a437f39ebf50","name":"luketask","description":"task_descrp","dateTime":"20168-05-25 14:25:00","userId":"17c85a78-cdbc-47cc-91ff-cc62e0acd610","status":"PENDING"}]


Get employee task:
curl -X GET -i http://localhost:8080/api/user/17c85a78-cdbc-47cc-91ff-cc62e0acd610/task/2f1f73d8-12e9-4b46-9c23-a437f39ebf50 --data ''

Result:
{"taskId":"2f1f73d8-12e9-4b46-9c23-a437f39ebf50","name":"luketask","description":"task_descrp","dateTime":"20168-05-25 14:25:00","userId":"17c85a78-cdbc-47cc-91ff-cc62e0acd610","status":"PENDING"}


Delete employees task:
curl -X DELETE -i http://localhost:8080/api/user/17c85a78-cdbc-47cc-91ff-cc62e0acd610/task/2f1f73d8-12e9-4b46-9c23-a437f39ebf50 --data ''

Result:
Status Code: 200
Content-Length: 0