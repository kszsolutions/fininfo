package BrokerApp.FinInfo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import BrokerApp.FinInfo.api.Employee;
import BrokerApp.FinInfo.api.TaskDetails;
/**
 * The full integration test for the fininfo application
 * @author luke keyser
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category(Integrationtest.class)
@DirtiesContext
public class FinInfoIntegrationTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mockMvc;

	@Autowired
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() {
		this.mockMvc = webAppContextSetup(context).build();
	}

	@Test
	public void employeeIntegration() throws IOException, Exception {
		// Test create employee
		Employee emp = new Employee("firstname", "lastname");
		MvcResult result = this.mockMvc.perform(post("/api/user").contentType(contentType).content(json(emp)))
				.andExpect(status().isCreated()).andReturn();
		String id = result.getResponse().getContentAsString().substring(12,
				result.getResponse().getContentAsString().length() - 2);
		
		// Retrieve employee
		this.mockMvc.perform(get("/api/user/")).andExpect(status().isOk());
		//Update employee
		Employee empUpdate = new Employee("firstname_update", "lastname_update");
		this.mockMvc.perform(put("/api/user/" + id).contentType(contentType).content(json(empUpdate)))
		.andExpect(status().isOk());
		
	}
	@Test
	public void taskIntegration() throws IOException, Exception {
		// Create  test employee
		Employee emp = new Employee("firstname", "lastname");
		MvcResult result = this.mockMvc.perform(post("/api/user").contentType(contentType).content(json(emp)))
				.andExpect(status().isCreated()).andReturn();
		String id = result.getResponse().getContentAsString().substring(12,
				result.getResponse().getContentAsString().length() - 2);
		
		//Add task
		Date now = new Date();
		TaskDetails task = new TaskDetails("junitName", "junitDescr", now);
		MvcResult taskResult = this.mockMvc.perform(post("/api/user/" + id + "/task").contentType(contentType).content(json(task)))
				.andExpect(status().isCreated()).andReturn();
		String taskId = taskResult.getResponse().getContentAsString().substring(12,
				taskResult.getResponse().getContentAsString().length() - 2);
		//Test update task
		TaskDetails taskUpdate = new TaskDetails("junitName_update", "junitDescr_update", now);
		this.mockMvc.perform(put("/api/user/" + id + "/task/" + taskId).contentType(contentType).content(json(taskUpdate)))
		.andExpect(status().isOk());
		
		//Test get employees tasks
		this.mockMvc.perform(get("/api/user/" + id + "/task"))
		.andExpect(status().isOk());
		//Test get employees task
		this.mockMvc.perform(get("/api/user/" + id + "/task/" + taskId))
		.andExpect(status().isOk());
		//Test delete task
		this.mockMvc.perform(delete("/api/user/" + id + "/task/" +  taskId))
		.andExpect(status().isOk());
	}

	private String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}
