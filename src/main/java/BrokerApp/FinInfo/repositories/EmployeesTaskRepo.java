package BrokerApp.FinInfo.repositories;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import BrokerApp.FinInfo.models.EmployeesTasks;

/**
 * Employee task repository
 * 
 * @author luke keyser
 *
 */
public interface EmployeesTaskRepo extends CrudRepository<EmployeesTasks, UUID> {

	EmployeesTasks findByTaskIdAndUserId(UUID taskId, UUID userId);

	void deleteByTaskIdAndUserId(UUID taskId, UUID userId);

	List<EmployeesTasks> findByUserId(UUID userId);

}
