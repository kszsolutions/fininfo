package BrokerApp.FinInfo.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import BrokerApp.FinInfo.models.Employees;

/**
 * Employee repository
 * 
 * @author luke keyser
 *
 */
public interface EmployeeRepo extends CrudRepository<Employees, UUID> {
	
	
	

}
