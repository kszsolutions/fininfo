package BrokerApp.FinInfo.api.errors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import BrokerApp.FinInfo.api.ErrorResponse;
import BrokerApp.FinInfo.exceptions.TaskNotFoundException;

/**
 * Custom error handler
 * 
 * @author luke keyser
 *
 */
@RestControllerAdvice
public class TaskNotFoundHandler {
	private static final String TASK_NOT_FOUND = "Task not found";

	@ExceptionHandler(TaskNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleNotFoundException(HttpServletRequest request, Exception ex) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(TASK_NOT_FOUND));

	}
}
