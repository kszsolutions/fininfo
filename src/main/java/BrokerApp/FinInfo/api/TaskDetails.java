package BrokerApp.FinInfo.api;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Task object as accepted via the REST call
 * 
 * @author luke keyser
 *
 */
public class TaskDetails {

	private String name;

	private String description;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date taskDate;

	@JsonCreator
	public TaskDetails(@JsonProperty("name") String name, @JsonProperty("description") String description,
			@JsonProperty("taskdate") Date taskDate) {
		this.name = name;
		this.description = description;
		this.taskDate = taskDate;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Date getTaskDate() {
		return taskDate;
	}

}
