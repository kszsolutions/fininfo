package BrokerApp.FinInfo.api.response;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeResponse {
	/**
	 * Returned response object for when a new employee is created
	 * 
	 * @author luke keyser
	 *
	 */
	@JsonProperty("user_id")
	private UUID userId;

	@JsonCreator
	public EmployeeResponse(@JsonProperty("user_id") UUID user_id) {
		this.userId = user_id;
	}

}
