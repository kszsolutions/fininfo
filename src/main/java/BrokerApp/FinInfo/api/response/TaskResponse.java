package BrokerApp.FinInfo.api.response;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaskResponse {
	/**
	 * Returned response object for when a new task is created
	 * 
	 * @author luke keyser
	 *
	 */
	@JsonProperty("task_id")
	private UUID taskId;

	@JsonCreator
	public TaskResponse(@JsonProperty("task_id") UUID taskId) {
		this.taskId = taskId;
	}

}
