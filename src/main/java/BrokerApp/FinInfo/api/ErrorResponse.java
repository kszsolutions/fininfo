package BrokerApp.FinInfo.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Custom error response
 * 
 * @author luke keyser
 *
 */
public class ErrorResponse {
	private final String type;

	public ErrorResponse(String type) {
		this.type = type;
	}

	@JsonProperty("error")
	public String getType() {
		return type;
	}
}
