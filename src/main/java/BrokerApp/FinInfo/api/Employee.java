package BrokerApp.FinInfo.api;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Employee object as accepted via the REST call
 * 
 * @author luke keyser
 *
 */
public class Employee {
	

	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;

	@JsonCreator
	public Employee(@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;

	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

}
