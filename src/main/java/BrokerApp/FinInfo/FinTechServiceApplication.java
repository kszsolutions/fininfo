package BrokerApp.FinInfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The starting application for fininfo broker app.
 * @author luke keyser
 *
 */

@SpringBootApplication
@EnableJpaRepositories
@EnableScheduling
public class FinTechServiceApplication extends SpringBootServletInitializer
{
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(FinTechServiceApplication.class);
	}
	
    public static void main( String[] args )
    {
        SpringApplication.run(FinTechServiceApplication.class, args);
    }
    
}
