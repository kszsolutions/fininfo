package BrokerApp.FinInfo.scheduledtasks;

import java.util.Date;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import BrokerApp.FinInfo.models.EmployeesTasks;
import BrokerApp.FinInfo.models.TaskStatus;
import BrokerApp.FinInfo.repositories.EmployeesTaskRepo;

/**
 * Sceduled task to check current status of the task and update the status. Runs
 * once a day
 * 
 * @author luke keyser
 *
 */
@Component
public class ScheduledTask {

	private static final Logger LOGGER = LogManager.getLogger(ScheduledTask.class);

	private final EmployeesTaskRepo employTaskRepo;

	@Inject
	public ScheduledTask(EmployeesTaskRepo employTaskRepo) {

		this.employTaskRepo = employTaskRepo;
	}

	@Scheduled(cron = "0 0 * * * *")
	public void updateTasks() {
		try {
			Date currentDate = new Date();
			Iterable<EmployeesTasks> getAllTasks = employTaskRepo.findAll();
			for (EmployeesTasks task : getAllTasks) {
				if (task.getDateTime().before(currentDate)) {
					task.setStatus(TaskStatus.DONE);
					employTaskRepo.save(task);
					LOGGER.info("Task id " + task.getTaskId() + " updated to done");
				}
			}
		}
		catch(Exception ex) {
				LOGGER.warn("Task scheduler failed error: " + ex);
			}
		

	

}
