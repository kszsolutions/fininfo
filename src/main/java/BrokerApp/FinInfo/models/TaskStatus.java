package BrokerApp.FinInfo.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum for task status
 * 
 * @author luke keyser
 *
 */
public enum TaskStatus {
	PENDING, DONE;

	@JsonValue
	public String toJson() {
		return this.toString();
	}

	@JsonCreator
	public static TaskStatus fromValue(String value) {
		return valueOf(value);
	}

}
