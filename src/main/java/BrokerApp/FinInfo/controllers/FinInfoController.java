package BrokerApp.FinInfo.controllers;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BrokerApp.FinInfo.api.Employee;
import BrokerApp.FinInfo.api.TaskDetails;
import BrokerApp.FinInfo.api.response.EmployeeResponse;
import BrokerApp.FinInfo.api.response.TaskResponse;
import BrokerApp.FinInfo.exceptions.EmployeeNotFoundException;
import BrokerApp.FinInfo.exceptions.TaskNotFoundException;
import BrokerApp.FinInfo.models.Employees;
import BrokerApp.FinInfo.models.EmployeesTasks;
import BrokerApp.FinInfo.services.FinInfoService;
import BrokerApp.FinInfo.services.FinInfoTaskService;

/**
 * This class is the REST controller for the FinInfo app
 * 
 * @author luke keyser
 *
 */
@RestController
@RequestMapping("/api/user")
public class FinInfoController {

	private static final Logger LOGGER = LogManager.getLogger();

	private final FinInfoService finService;
	private final FinInfoTaskService taskService;

	@Inject
	public FinInfoController(FinInfoService finService, FinInfoTaskService taskService) {
		this.finService = finService;
		this.taskService = taskService;
	}

	/**
	 * REST end point to create new employee that accepts Employee details
	 * 
	 * @param details
	 * @return employees user id
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<EmployeeResponse> addEmployee(@RequestBody @Valid Employee details) throws Exception {
		LOGGER.info("Add employee");
		Employees employee = finService.addUser(details);
		EmployeeResponse response = new EmployeeResponse(employee.getUserId());
		return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}

	/**
	 * REST end point to retrieve a list of employees
	 * 
	 * @return iterable of employees
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Iterable<Employees>> getEmployees() throws Exception {
		LOGGER.info("Get list of employees");
		Iterable<Employees> employee = finService.getListOfEmployees();
		return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").body(employee);
	}

	/**
	 * REST end point to edit an employee accepts user id and Employee details
	 * 
	 * @param id
	 * @param details
	 * @return http status response
	 * @throws EmployeeNotFoundException
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity editEmployee(@PathVariable(value = "id") String id, @RequestBody Employee details)
			throws EmployeeNotFoundException {
		LOGGER.info("Edit employee");
		Employees employee = finService.updateEmployee(id, details);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	/**
	 * REST end point to and new task for an employee that accepts a user id and
	 * TaskDetails details
	 * 
	 * @param userId
	 * @param details
	 * @return task id
	 */
	@RequestMapping(path = "/{user_id}/task", method = RequestMethod.POST)
	public ResponseEntity<TaskResponse> addNewTask(@PathVariable(value = "user_id") String userId,
			@RequestBody TaskDetails details) {
		LOGGER.info("Add task");
		EmployeesTasks task = taskService.createNewTask(UUID.fromString(userId), details);
		TaskResponse response = new TaskResponse(task.getTaskId());
		return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}

	/**
	 * REST end point to update a users task that accepts an user id and TaskDetails
	 * details
	 * 
	 * @param userId
	 * @param taskId
	 * @param details
	 * @return http status
	 * @throws TaskNotFoundException
	 */
	@RequestMapping(path = "/{user_id}/task/{task_id}", method = RequestMethod.PUT)
	public ResponseEntity updateTask(@PathVariable(value = "user_id") String userId,
			@PathVariable(value = "task_id") String taskId, @RequestBody TaskDetails details)
			throws TaskNotFoundException {
		LOGGER.info("Update task");
		taskService.updateTask(UUID.fromString(taskId), UUID.fromString(userId), details);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	/**
	 * REST end point to delete an users task that accepts an user id
	 * 
	 * @param userId
	 * @param taskId
	 * @return http status
	 * @throws TaskNotFoundException
	 */
	@RequestMapping(path = "/{user_id}/task/{task_id}", method = RequestMethod.DELETE)
	public ResponseEntity deleteTask(@PathVariable(value = "user_id") String userId,
			@PathVariable(value = "task_id") String taskId) throws TaskNotFoundException {
		LOGGER.info("Delete task");
		taskService.deleteTask(UUID.fromString(taskId), UUID.fromString(userId));
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	/**
	 * REST end point to get a list of employees tasks accepts an user id
	 * 
	 * @param userId
	 * @return list of employees tasks
	 * @throws TaskNotFoundException
	 */
	@RequestMapping(path = "/{user_id}/task", method = RequestMethod.GET)
	public ResponseEntity<List<EmployeesTasks>> getEmployeesTasks(@PathVariable(value = "user_id") String userId)
			throws TaskNotFoundException {
		LOGGER.info("Get list of employees tasks");
		List<EmployeesTasks> employeeTasks = taskService.getListOfEmployeesTasks(UUID.fromString(userId));
		return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").body(employeeTasks);
	}

	/**
	 * REST end point to retrieve a task for an employee accepts an user id
	 * 
	 * @param userId
	 * @param taskId
	 * @return employee's tasks
	 * @throws TaskNotFoundException
	 */
	@RequestMapping(path = "/{user_id}/task/{task_id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeesTasks> getEmployeesTask(@PathVariable(value = "user_id") String userId,
			@PathVariable(value = "task_id") String taskId) throws TaskNotFoundException {
		LOGGER.info("Get employees task");
		EmployeesTasks employeeTasks = taskService.getEmployeesTask(UUID.fromString(taskId), UUID.fromString(userId));
		return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").body(employeeTasks);

	}
}