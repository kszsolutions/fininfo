package BrokerApp.FinInfo.services;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import BrokerApp.FinInfo.api.Employee;
import BrokerApp.FinInfo.exceptions.EmployeeNotFoundException;
import BrokerApp.FinInfo.models.Employees;
import BrokerApp.FinInfo.repositories.EmployeeRepo;

/**
 * This the main service for the employee object
 * 
 * @author luke keyser
 *
 */
@Service
public class FinInfoService {

	private final EmployeeRepo employRepo;

	@Inject
	public FinInfoService(EmployeeRepo employRepo) {

		this.employRepo = employRepo;
	}

	/**
	 * Creater a new user
	 * 
	 * @param details
	 * @return employee object
	 */
	public Employees addUser(Employee details) {

		Employees employee = new Employees();
		employee.setFirstName(details.getFirstName());
		employee.setLastName(details.getLastName());
		employee = employRepo.save(employee);
		return employee;
	}

	/**
	 * Retrieves a iterable of employees
	 * 
	 * @return iterable employee object
	 */
	public Iterable<Employees> getListOfEmployees() {
		Iterable<Employees> employeesList = employRepo.findAll();
		return employeesList;
	}

	/**
	 * Update an existing employee
	 * 
	 * @param id
	 * @param details
	 * @return employee object
	 * @throws EmployeeNotFoundException
	 */
	public Employees updateEmployee(String id, Employee details) throws EmployeeNotFoundException {
		UUID userId = UUID.fromString(id);
		Employees employeeUpdate = employRepo.findOne(userId);
		if (!Employees.class.isInstance(employeeUpdate)) {
			throw new EmployeeNotFoundException();
		}
		employeeUpdate.setFirstName(details.getFirstName());
		employeeUpdate.setLastName(details.getLastName());
		employeeUpdate = employRepo.save(employeeUpdate);
		return employeeUpdate;

	}

}
