package BrokerApp.FinInfo.services;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import BrokerApp.FinInfo.api.TaskDetails;
import BrokerApp.FinInfo.exceptions.TaskNotFoundException;
import BrokerApp.FinInfo.models.EmployeesTasks;
import BrokerApp.FinInfo.models.TaskStatus;
import BrokerApp.FinInfo.repositories.EmployeesTaskRepo;

/**
 * This is the main service for the tasks details
 * 
 * @author luke keyser
 *
 */
@Service
public class FinInfoTaskService {

	private final EmployeesTaskRepo employTaskRepo;

	@Inject
	public FinInfoTaskService(EmployeesTaskRepo employTaskRepo) {

		this.employTaskRepo = employTaskRepo;
	}

	/**
	 * Create new task for an employee
	 * 
	 * @param userId
	 * @param details
	 * @return employeetasks object
	 */
	public EmployeesTasks createNewTask(UUID userId, TaskDetails details) {
		EmployeesTasks task = new EmployeesTasks();
		task.setDateTime(details.getTaskDate());
		task.setDescription(details.getDescription());
		task.setName(details.getName());
		task.setStatus(TaskStatus.PENDING);
		task.setUserId(userId);
		task = employTaskRepo.save(task);
		return task;
	}

	/**
	 * Updates an existing employees task
	 * 
	 * @param taskId
	 * @param userId
	 * @param details
	 * @throws TaskNotFoundException
	 */
	public void updateTask(UUID taskId, UUID userId, TaskDetails details) throws TaskNotFoundException {
		EmployeesTasks task = employTaskRepo.findByTaskIdAndUserId(taskId, userId);
		if (!EmployeesTasks.class.isInstance(task)) {
			throw new TaskNotFoundException();
		}
		if (details.getName() != null) {
			task.setName(details.getName());
		}
		if (details.getDescription() != null) {
			task.setDescription(details.getDescription());
		}
		if (details.getTaskDate() != null) {
			task.setDateTime(details.getTaskDate());
		}
		employTaskRepo.save(task);

	}

	/**
	 * Deletes an existing take for an employee
	 * 
	 * @param taskId
	 * @param userId
	 */
	@Transactional
	public void deleteTask(UUID taskId, UUID userId) {
		employTaskRepo.deleteByTaskIdAndUserId(taskId, userId);
	}

	public List<EmployeesTasks> getListOfEmployeesTasks(UUID userId) throws TaskNotFoundException {
		List<EmployeesTasks> employeesTaskList = employTaskRepo.findByUserId(userId);
		if (employeesTaskList == null || employeesTaskList.size() == 0) {
			throw new TaskNotFoundException();
		}
		return employeesTaskList;
	}

	/**
	 * Retrieves an employees task
	 * 
	 * @param taskId
	 * @param userId
	 * @return employeetasks object
	 * @throws TaskNotFoundException
	 */
	public EmployeesTasks getEmployeesTask(UUID taskId, UUID userId) throws TaskNotFoundException {
		EmployeesTasks task = employTaskRepo.findByTaskIdAndUserId(taskId, userId);
		if (!EmployeesTasks.class.isInstance(task)) {
			throw new TaskNotFoundException();
		}
		return task;
	}

}
